var aTranslations;

aTranslations = new Array();

aTranslations[0]  = new Array(32513, "en-US",  "Ranked Match");
aTranslations[1]  = new Array(32513, "ja-JP",  "ランク マッチ");
aTranslations[2]  = new Array(32513, "de-DE",  "Ranglisten-Spiel");
aTranslations[3]  = new Array(32513, "fr-FR",  "Partie avec Classement");
aTranslations[4]  = new Array(32513, "es-ES",  "Partida Igualada");
aTranslations[5]  = new Array(32513, "it-IT",  "Partita Classificata");
aTranslations[6]  = new Array(32513, "ko-KR",  "랭킹 매치");
aTranslations[7]  = new Array(32513, "zh-CHT", "等級配對");
aTranslations[8]  = new Array(32513, "pt-PT",  "Partida por Ranking");

aTranslations[9]  = new Array(32514, "en-US",  "Player Match");
aTranslations[10] = new Array(32514, "ja-JP",  "プレイヤー マッチ");
aTranslations[11] = new Array(32514, "de-DE",  "Mitspieler-Suche");
aTranslations[12] = new Array(32514, "fr-FR",  "Mise en Relation");
aTranslations[13] = new Array(32514, "es-ES",  "Partida de Jugador");
aTranslations[14] = new Array(32514, "it-IT",  "Partita del Giocatore");
aTranslations[15] = new Array(32514, "ko-KR",  "플레이어 매치");
aTranslations[16] = new Array(32514, "zh-CHT", "玩家配對");
aTranslations[17] = new Array(32514, "pt-PT",  "Partida por Jogador");

var oXmlDoc;
var args;
var bFileModified = false;

function Usage()
{
    WScript.Echo("cscript FixTranslations.js <filename>");
    WScript.Quit();
}

function CheckLocalizedStringTranslation(id, oLocalizedStringTranslation)
{
    var translationLocale = oLocalizedStringTranslation.getAttribute("locale");
    var iCurrentTranslation;
    
    for(iCurrentTranslation = 0; iCurrentTranslation < aTranslations.length; ++iCurrentTranslation)
    {
        if(aTranslations[iCurrentTranslation][0] == id && 
           aTranslations[iCurrentTranslation][1] == translationLocale &&
           aTranslations[iCurrentTranslation][2] != oLocalizedStringTranslation.text)
        {
            oLocalizedStringTranslation.text = aTranslations[iCurrentTranslation][2];
            bFileModified = true;
            return;
        }
    }
}

function CheckLocalizedString(oLocalizedStringElement)
{
    var id = oLocalizedStringElement.getAttribute("id");
    
    var translationNodes = oLocalizedStringElement.selectNodes("Translation");
    var translationEnum = new Enumerator(translationNodes);
    
    for(; !translationEnum.atEnd(); translationEnum.moveNext())
    {
        CheckLocalizedStringTranslation(id, translationEnum.item());
    }
}

function ReplaceStrings(oXmlDoc)
{
    var oNodesToFix = oXmlDoc.selectNodes("//LocalizedString");
    
    var localizedStringEnum = new Enumerator(oNodesToFix);
    
    for(; !localizedStringEnum.atEnd(); localizedStringEnum.moveNext())
    {
        CheckLocalizedString(localizedStringEnum.item());
    }
}

try
{
    args = WScript.Arguments;
    if(args.length != 1)
    {
        Usage();
    }

    oXmlDoc = new ActiveXObject("MSXML.DOMDocument");

    if(!oXmlDoc.load(args.Item(0)))
    {
        WScript.Echo("Failed loading document " + args.Item(0) + ".  Please verify the name supplied.");
        WScript.Quit();
    }

    ReplaceStrings(oXmlDoc);

    if(bFileModified)
    {
        oXmlDoc.save(args.Item(0));        
        WScript.Echo("Success.  Translations have been fixed.");
    }
    else
    {
        WScript.Echo("No translations needed fixing.  The file was not modified.");
    }
}
catch(e)
{
    WScript.Echo("Failed.  Exception thrown: " + e);
}
    
    




