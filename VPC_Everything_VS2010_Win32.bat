::===================================================
:: Generates the Everything vpc group and creates solution
:: crack 12/17/2020
::===================================================

@echo off
:setlocal

push %~dp0

echo Generating projects for Everything (Win32/VS2010)


devtools\bin\vpc +everything -physics /2010 /hl2 /cstrike /dod /hl2mp /episodic /tf /portal /hl1 /lostcoast /mksln Everything_VS2010_Win32.sln

echo Completed.

exit /b 0